import os
import requests
from datetime import datetime, timedelta

from dotenv import load_dotenv

load_dotenv()

def convert_unix_to_readable(unix_time, timezone):
    utc_time = datetime.utcfromtimestamp(unix_time)
    local_time = utc_time + timedelta(seconds=timezone)
    return local_time.strftime('%H:%M:%S')

def get_weather_data(api_key):
    city_id = os.getenv('CITY_ID')
    url = f"http://api.openweathermap.org/data/2.5/forecast?id={city_id}&appid={api_key}&units=metric"
    response = requests.get(url)
    return response.json()

def extract_today_forecast(weather_data):
    today = datetime.now().date()
    today_forecast = [f for f in weather_data['list'] if datetime.fromtimestamp(f['dt']).date() == today]

    return today_forecast

def get_weather_icon_url(weather_data):
    if "weather" in weather_data and len(weather_data["weather"]) > 0:
        icon_id = weather_data["weather"][0]["icon"]
        icon_url = f"http://openweathermap.org/img/wn/{icon_id}@2x.png"
        return icon_url
    else:
        return "https://png.pngtree.com/png-clipart/20210201/ourmid/pngtree-beautiful-blue-sky-on-a-transparent-background-png-image_2878914.jpg"

def send_telegram_message(bot_token, chat_id, message, photo_url=None):
    if photo_url:
        photo_data = {
            "chat_id": chat_id,
            "photo": photo_url
        }
        url = f"https://api.telegram.org/bot{bot_token}/sendPhoto"
        response = requests.post(url, data=photo_data)
    
    url = f"https://api.telegram.org/bot{bot_token}/sendMessage"
    data = {
        "chat_id": chat_id,
        "text": message
    }
    response = requests.post(url, data=data)

def main():
    weather_api_key = os.getenv('KEY_WEATHER')
    weather_data = get_weather_data(weather_api_key)

    timezone = weather_data['city']['timezone']
    sunrise_time = convert_unix_to_readable(weather_data['city']['sunrise'], timezone)
    sunset_time = convert_unix_to_readable(weather_data['city']['sunset'], timezone)

    day_data = extract_today_forecast(weather_data)
    day_data = day_data[0]

    weather_message = (
        f"Weather in {weather_data['city']['name']}\n"
        f"Current weather: {day_data['weather'][0]['description']}\n"
        f"-------Temperature------\n"
        f"min: {day_data['main']['temp_min']}°C\n"
        f"max: {day_data['main']['temp_max']}°C\n"
        f"Feels like: {day_data['main']['feels_like']}°C\n"
        f"\n"
        f"Wind speed: {day_data['wind']['speed']} m/s\n"
        f"Sunrise: {sunrise_time}\n"
        f"Sunset: {sunset_time}\n"
                       )

    if hasattr(day_data, 'rain'):
        weather_message += day_data['rain']['3h']

    bot_token = os.getenv('BOT_TOKEN')
    chat_id = os.getenv('CHAT_ID')

    url_img = get_weather_icon_url(day_data)

    send_telegram_message(bot_token, chat_id, weather_message, url_img)

if __name__ == '__main__':
    main()